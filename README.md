# JSON filtertree

A Python script to load and filter a JSON file, showing the nested-key structure with regex filtering.

## Usage:

```
    json_filtertree <JSONfile> [...filters...]
```


The first argument should be the JSON filename.

The following filter arguments are optional and are one or more of:

- `+regex` : any key in the path must match the regex (like "grep")
- `-regex` : no key in the path can match the regex (like "grep -v")
- `$regex` : the final key in the path must match the regex (note: you may have to escape the $ in your terminal)
- `^regex` : the first key in the path must match the regex

`regex` means Regular Expression. While you can use simple strings, these will be interpreted as regular expressions. If you are not familiar with these, you might want to start at https://en.wikipedia.org/wiki/Regular_expression .


Requires various standard Python modules that, if not available, are easily
installed with pip. Requires Python 3 and initially tested on 3.8 and 3.9.



Developed for use with binary_c-python (https://gitlab.eps.surrey.ac.uk/ri0005/binary_c-python) but probably useful elsewhere.
(c) Robert Izzard 2021
